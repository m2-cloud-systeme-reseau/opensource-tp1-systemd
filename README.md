# OpenSource TP1 - Systemd

Sujet : https://gitlab.com/it4lik/master-opensource-2019/blob/master/tp/tp1-systemd.md

Environnement utilisé : Vagrant. (cf. [Vagrantfile](./Vagrantfile))

# `systemd`-Basics
Aujourd'hui nous nous penchons sur `systemd`, ici en `v243.4-1.fc31`.
`systemd` vient remplacer le processus `init` habituellement présent sur la plupart des distributions Linux. Il est le premier processus lancé sur la machine, et se charge de lancer tous les autres :
```
[root@fedora31 ~]# ps -ef | grep systemd
root           1       0  0 06:32 ?        00:00:03 /usr/lib/systemd/systemd --switched-root --system --deserialize 29
root         441       1  0 06:32 ?        00:00:00 /usr/lib/systemd/systemd-journald
root         443       1  0 06:32 ?        00:00:00 /usr/lib/systemd/systemd-udevd
root         555       1  0 06:32 ?        00:00:00 /usr/lib/systemd/systemd-logind
papy         694       1  0 06:33 ?        00:00:00 /usr/lib/systemd/systemd --user
systemd+    2438       1  0 09:52 ?        00:00:00 /usr/lib/systemd/systemd-networkd
systemd+    2982       1  0 11:38 ?        00:00:00 /usr/lib/systemd/systemd-resolved
root        3562    3352  0 12:57 pts/0    00:00:00 grep --color=auto systemd
[root@fedora31 ~]# pstree
systemd─┬─VBoxService───7*[{VBoxService}]
        ├─agetty
        ├─auditd───{auditd}
        ├─containerd───10*[{containerd}]
        ├─dbus-broker-lau───dbus-broker
        ├─dockerd───9*[{dockerd}]
        ├─firewalld───{firewalld}
        ├─haveged
        ├─memcached───9*[{memcached}]
        ├─polkitd───5*[{polkitd}]
        ├─sshd───sshd───sshd───bash───sudo───su───bash───pstree
        ├─sssd─┬─sssd_be
        │      └─sssd_nss
        ├─systemd───(sd-pam)
        ├─systemd-journal
        ├─systemd-logind
        ├─systemd-network
        ├─systemd-resolve
        ├─systemd-udevd
        └─tuned───3*[{tuned}]
```
On peut voir que systemd a le PID **1**, et est le parent des différents processus systèmes de la machine :
* **agetty** est le binaire qui s'occuper d'alouer des terminaux virtuels aux utilisateurs voulant interagir avec le système.
* **auditd** est l'outil de journalisation des appels systèmes, entre autres. Il est responsable de la traçabilité des opérations exécutées sur le système. `auditd` est la partie "user space" du Linux Audit framework, qui possède aussi des composants directement dans le kernel.
* **dbus-brocker** est responsable des communications inter-processus.
* **firewalld** est une surcouche à `iptables` permettant de piloter le module `netfilter` du kernel.
* **haveged** génère de l'entropie et de l'aléatoire pour les applications qui en ont besoin. C'est ce processus qui répond lorsqu'on affiche le fichier `/dev/urandom` par exemple.
* **memcached** est un démon système permettant de stocker des couples clé/valeur en RAM, pratique pour faire du cache de fichier par exemple.
* **polkitd** permet aux applications s'exécutant avec des droits restreints d'interagir avec des services privilégiés du système. C'est une alternative à la méthode traditionnelle d'élévation de privilège via `sudo` par exemple.
* **sd-pam** est le module côté userspace responsable de l'application des permissions UNIX sur le système.
* **systemd-journal** est le démon responsable de la journalisation des unités `systemd`.
* **systemd-logind** est responsable de la gestion des sessions utilisateurs.
* **systemd-netdowk** est en charge de la configuration réseau du système
* **systemd-resolve** est le service système de résolution de nom.
* **systemd-udev** est le gestionnaire de périphériques du système.
* **tuned** est un utlitaire de RedHat permettant d'optimiser l'utilisation des ressources systèmes.
* **sshd**, **containerd** et **dockerd** sont des exemples d'unités de type service gérées par `systemd`.


## Gestion du temps

`timedatectl` est l'outil de `systemd` permettant de gérer la date et l'heure du système. Il gère la synchronisation de l'horloge du système sur des sources externes.
```
[root@tp1-systemd ~]# timedatectl
               Local time: Fri 2019-11-29 11:33:18 UTC
           Universal time: Fri 2019-11-29 11:33:18 UTC
                 RTC time: Fri 2019-11-29 11:33:16
                Time zone: UTC (UTC, +0000)
```
Il existe plusieurs façons de décrire la date et l'heure :
* **Universal Time (UTC)**: C'est le temps de référence, définit pour les régions suivant le méridien de Greenwich, en Angleterre.
* **Local Time**: Le local time se base sur le UTC, et vient le pondérer en fonction de la timezone dans laquelle nous nous trouvons sur le globe. 
* **Real Time (RTC)**: Cette horloge est beaucoup plus précise que les 2 précédentes, et est souvent intégrée à la machine elle-même, souvent de manière physique sur sa carte mère. Sa particularité est de continuer à fonctionner lorsque le système est éteint.

Changement de Time Zone :
```
[root@tp1-systemd ~]# timedatectl set-timezone "Europe/Paris"
[root@tp1-systemd ~]# timedatectl
               Local time: Fri 2019-11-29 12:34:28 CET
           Universal time: Fri 2019-11-29 11:34:28 UTC
                 RTC time: Fri 2019-11-29 11:34:26
                Time zone: Europe/Paris (CET, +0100)
System clock synchronized: yes
              NTP service: active
          RTC in local TZ: yes
```
On note que le service NTP semble activé. Pour le désactiver :
```
[root@tp1-systemd ~]# timedatectl set-ntp false
[root@tp1-systemd ~]# timedatectl
               Local time: Fri 2019-11-29 11:45:44 UTC
           Universal time: Fri 2019-11-29 11:45:44 UTC
                 RTC time: Fri 2019-11-29 11:45:42
                Time zone: UTC (UTC, +0000)
System clock synchronized: yes
              NTP service: inactive
          RTC in local TZ: yes
```

## Gestion de noms
La gestion de nom est aussi gérée par `systemd` avec `hostnamectl`.
En réalité, `hostnamectl` peut récupérer plus d'informations que simplement le nom de domaine :
```
[root@tp1-systemd ~]# hostnamectl
   Static hostname: tp1-systemd.lab
         Icon name: computer-vm
           Chassis: vm
        Machine ID: 01295c897811417fafae471085f15b85
           Boot ID: 089a5117248747fabef9d999a79a9e9d
    Virtualization: oracle
  Operating System: Fedora 31 (Thirty One)
       CPE OS Name: cpe:/o:fedoraproject:fedora:31
            Kernel: Linux 5.3.11-300.fc31.x86_64
      Architecture: x86-64
```
Il permet également de modifier le hostname de la machine :
```
[root@tp1-systemd ~]# hostnamectl set-hostname os-tp1-systemd
[root@tp1-systemd ~]# hostname 
os-tp1-systemd
```
`systemd` permet plusieurs types de hostname, décrits par des options à `hostnamectl set-hostname` :
* `--static` est le hostname "classique", renseigné dans /etc/hostname
* `--pretty` est un hostname informatif supportant des charactères UTF-8
* `--transient` est un hostname dynamique géré par le kernel. Il peut être modifié au runtime, par exemple par DHCP.
Pour des machines de prod, le nom statique est tout indiqué, mais le nom `pretty` peut permettre une description plus précise des services qui tournent l'hôte.

Enfin, `hostnamectl set-deployment <env>` permet de rajouter encore une métadonnée sur l'hôte en renseignant un environnement dans lequel il se situe, par exemple "prod" ou "staging". 

## Gestion du réseau et de la résolution de noms
Pour gérer la stack réseau, deux outils sont livrés avec systemd :
* `NetworkManager`
    * souvent activé par défaut
    * réagit dynamiquement aux changements du réseau (mise à jour de `/etc/resolv.conf` en fonction des réseaux connectés par exemple)
    * idéal pour un déploiement desktop
    * expose une API dbus
* `systemd-networkd`
    * permet une grande flexibilité de configuration
        * configuration de plusieurs interfaces simultanément (wildcards)
        * fonctionnalités avancées
    * utilise une syntaxe standard `systemd`
    * complètement intégré à `systemd` (gestion, logs, etc)
    * idéal en déploiement cloud

### `NetworkManager` 
Récupération du bail DHCP avec `NetworkManager` :
```
# papy at La-Bretzelerie in /~ :
nmcli -f DHCP4 con show "WiFi@YNOV"
DHCP4.OPTION[1]:                        dhcp_lease_time = 3600
DHCP4.OPTION[2]:                        dhcp_rebinding_time = 3150
DHCP4.OPTION[3]:                        dhcp_renewal_time = 1800
DHCP4.OPTION[4]:                        dhcp_server_identifier = 10.33.3.254
DHCP4.OPTION[5]:                        domain_name = auvence.co
DHCP4.OPTION[6]:                        domain_name_servers = 10.33.10.20 10.33.10.2 8.8.8.8 8.8.4.4
DHCP4.OPTION[7]:                        expiry = 1575038978
DHCP4.OPTION[8]:                        ip_address = 10.33.2.54
DHCP4.OPTION[9]:                        requested_broadcast_address = 1
DHCP4.OPTION[10]:                       requested_dhcp_server_identifier = 1
DHCP4.OPTION[11]:                       requested_domain_name = 1
DHCP4.OPTION[12]:                       requested_domain_name_servers = 1
DHCP4.OPTION[13]:                       requested_domain_search = 1
DHCP4.OPTION[14]:                       requested_host_name = 1
DHCP4.OPTION[15]:                       requested_interface_mtu = 1
DHCP4.OPTION[16]:                       requested_ms_classless_static_routes = 1
DHCP4.OPTION[17]:                       requested_nis_domain = 1
DHCP4.OPTION[18]:                       requested_nis_servers = 1
DHCP4.OPTION[19]:                       requested_ntp_servers = 1
DHCP4.OPTION[20]:                       requested_rfc3442_classless_static_routes = 1
DHCP4.OPTION[21]:                       requested_root_path = 1
DHCP4.OPTION[22]:                       requested_routers = 1
DHCP4.OPTION[23]:                       requested_static_routes = 1
DHCP4.OPTION[24]:                       requested_subnet_mask = 1
DHCP4.OPTION[25]:                       requested_time_offset = 1
DHCP4.OPTION[26]:                       requested_wpad = 1
DHCP4.OPTION[27]:                       routers = 10.33.3.253
DHCP4.OPTION[28]:                       subnet_mask = 255.255.252.0
```

### `systemd-networkd`
Le snippet suivant montre comment désactiver `NetworkManager` au profit de `systemd-networkd` :
```
[root@tp1-systemd ~]# systemctl disable NetworkManager && systemctl enable systemd-networkd
[root@tp1-systemd ~]# echo "# Vbox Host-only interface static config
> [Match]
> Name=eth1
> 
> [Network]
> DHCP=no
> Address=192.168.100.22/24
> DNS=1.1.1.1
> 
> [Route]
> Gateway=192.168.100.254" > /etc/systemd/network/eth1.network
[root@tp1-systemd ~]# systemctl stop NetworkManager && systemctl start systemd-networkd
```
> Note: 
> Cette opération a été réalisée via SSH : **aucune interruption de la connexion**
> la doc officielle renseigne **beaucoup** de champs de configuration utilisables pour décrire une configuration réseau très poussée et flexible, et ce, sur n'importe quelle distribution utilisant `systemd`. Une manière **standardisée et cross-distrib** de définir de la configuration réseau. :fire:

### `systemd-resolved`
On active le service de résolution de nom de `systemd` :
```
[root@tp1-systemd ~]# systemctl enable --now systemd-resolved
Created symlink /etc/systemd/system/dbus-org.freedesktop.resolve1.service → /usr/lib/systemd/system/systemd-resolved.service.
Created symlink /etc/systemd/system/multi-user.target.wants/systemd-resolved.service → /usr/lib/systemd/system/systemd-resolved.service.
[root@tp1-systemd ~]# systemctl status systemd-resolved
● systemd-resolved.service - Network Name Resolution
   Loaded: loaded (/usr/lib/systemd/system/systemd-resolved.service; enabled; vendor preset: disabled)
   Active: active (running) since Fri 2019-11-29 14:44:14 UTC; 7s ago
     Docs: man:systemd-resolved.service(8)
           https://www.freedesktop.org/wiki/Software/systemd/resolved
           https://www.freedesktop.org/wiki/Software/systemd/writing-network-configuration-managers
           https://www.freedesktop.org/wiki/Software/systemd/writing-resolver-clients
 Main PID: 2472 (systemd-resolve)
   Status: "Processing requests..."
    Tasks: 1 (limit: 2354)
   Memory: 11.4M
   CGroup: /system.slice/systemd-resolved.service
           └─2472 /usr/lib/systemd/systemd-resolved
[root@tp1-systemd ~]# ss -lapunt | grep 53
udp    UNCONN  0       0           127.0.0.53%lo:53              0.0.0.0:*       users:(("systemd-resolve",pid=2472,fd=18))                                     
udp    UNCONN  0       0                 0.0.0.0:5355            0.0.0.0:*       users:(("systemd-resolve",pid=2472,fd=12))                                     
udp    UNCONN  0       0                    [::]:5355               [::]:*       users:(("systemd-resolve",pid=2472,fd=14))                                     
tcp    LISTEN  0       128               0.0.0.0:5355            0.0.0.0:*       users:(("systemd-resolve",pid=2472,fd=13))                                     
tcp    LISTEN  0       128         127.0.0.53%lo:53              0.0.0.0:*       users:(("systemd-resolve",pid=2472,fd=19))                                     
tcp    LISTEN  0       128                  [::]:5355               [::]:*       users:(("systemd-resolve",pid=2472,fd=15)) 
```
On remarque que les services DNS sur le port 53 n'écotent qu'en local (`127.0.0.53%lo`). On peut donc l'interroger :
```
root@tp1-systemd ~]# dig google.com @127.0.0.53

; <<>> DiG 9.11.13-RedHat-9.11.13-2.fc31 <<>> google.com @127.0.0.53
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 48048
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 65494
;; QUESTION SECTION:
;google.com.			IN	A

;; ANSWER SECTION:
google.com.		3	IN	A	216.58.215.46

;; Query time: 68 msec
;; SERVER: 127.0.0.53#53(127.0.0.53)
;; WHEN: Fri Nov 29 14:58:17 UTC 2019
;; MSG SIZE  rcvd: 55

[root@tp1-systemd ~]# systemd-resolve google.com
google.com: 216.58.215.46                      -- link: eth0

-- Information acquired via protocol DNS in 27.4ms.
-- Data is authenticated: no
```

On remplace donc le fichier `/etc/resolv.conf` par un lien symbolique vers le fichier dynamique généré par `systemd-resolved`. Ainsi, toutes les requêtes DNS passeront par notre serveur local
```
[root@tp1-systemd ~]# sudo rm -rf /etc/resolv.conf
[root@tp1-systemd ~]# ln -s /run/systemd/resolve/stub-resolv.conf /etc/resolv.conf
```
La configuration de ce serveur DNS est présente dans `/etc/systemd/resolved.conf` :
```
[root@tp1-systemd ~]# cat /etc/systemd/resolved.conf
[Resolve]
DNS=1.1.1.1
FallbackDNS=8.8.8.8 1.0.0.1 8.8.4.4 2606:4700:4700::1111 2001:4860:4860::8888 2606:4700:4700::1001 2001:4860:4860::8844
[root@tp1-systemd ~]# systemctl restart systemd-resolved
[root@tp1-systemd ~]# resolvectl 
Global
       LLMNR setting: yes
MulticastDNS setting: yes
  DNSOverTLS setting: no
      DNSSEC setting: allow-downgrade
    DNSSEC supported: yes
         DNS Servers: 1.1.1.1
Fallback DNS Servers: 8.8.8.8
                      1.0.0.1
                      8.8.4.4
[...]
```

#### DNS over TLS
Pour activer DNS over TLS, il suffit de :
* renseigner dans le champs "DNS" de `/etc/systemd/resolved.conf` un serveur DNS compatible DNS over TLS (obviously)
* Renseigner un champs "DNSoverTLS" à `yes` ou `opportunistic` dans le même fichier.

Le serveur écoutera sur le port 853.
```
[root@os-tp1-systemd ~]$ cat /etc/systemd/resolved.conf 
[Resolve]
DNS=1.1.1.1
FallbackDNS=8.8.8.8 1.0.0.1 8.8.4.4 2606:4700:4700::1111 2001:4860:4860::8888 2606:4700:4700::1001 2001:4860:4860::8844
DNSOverTLS=yes

[root@os-tp1-systemd ~]$ tcpdump -n -nn port 853
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on eth0, link-type EN10MB (Ethernet), capture size 262144 bytes
16:07:37.443018 IP 10.0.2.15.55678 > 1.1.1.1.853: Flags [S], seq 2056634984, win 64240, options [mss 1460,sackOK,TS val 2606888994 ecr 0,nop,wscale 7,tfo  cookiereq,nop,nop], length 0
16:07:37.463171 IP 1.1.1.1.853 > 10.0.2.15.55678: Flags [S.], seq 2431872001, ack 2056634985, win 65535, options [mss 1460], length 0
[...]
```

## `logind`

`loggind` est le module de `systemd` qui va gérer les différentes sessions utilisateurs. Par exemple, j'ai actuellement 2 sessions SSH ouvertes sur ma machine :
```
[root@os-tp1-systemd ~]# loginctl
SESSION  UID USER SEAT TTY  
      3 1001 papy      pts/0
      4 1001 papy      pts/1

2 sessions listed.
```
On peut par exemple connaître rapidement les utilisateurs actuellement connectés à la machine :
```
[root@tp1-systemd ~]# loginctl list-users
 UID USER
1001 papy

1 users listed.
```
On trouve quelques options bien sympathiques comme le `-H`, permettant de lancer la commande sur un hôte distant (*very* UNIX-compliant :trollface:)


## Gestion d'unités basiques (services)
`systemctl` est l'outil de `systemd` permettant de gérer des **unités de service `systemd`**. Cette notion centrale désigne n'importe quelle ressource gérée par `systemd`. Elle peuvent être de plusieurs types, comme des *services*, mais aussi des *targets*, des *timers*, des *sockets*, des block et network *devices*, etc...

Pour récupérer des infos sur une unité de service, on peut utiliser `systemctl` :
```
[root@tp1-systemd ~]# systemctl list-units -t service | grep chronyd
[root@tp1-systemd ~]# systemctl status chronyd
● chronyd.service - NTP client/server
   Loaded: loaded (/usr/lib/systemd/system/chronyd.service; disabled; vendor preset: enabled)
   Active: inactive (dead)
     Docs: man:chronyd(8)
           man:chrony.conf(5)

Nov 29 10:55:09 tp1-systemd.lab systemd[1]: Started NTP client/server.
Nov 29 10:55:17 tp1-systemd.lab chronyd[528]: Selected source 176.31.101.9
Nov 29 10:55:17 tp1-systemd.lab chronyd[528]: System clock TAI offset set to 37 seconds
Nov 29 10:55:17 tp1-systemd.lab chronyd[528]: System clock wrong by 1.164654 seconds, adjustment started
Nov 29 10:55:18 tp1-systemd.lab chronyd[528]: System clock was stepped by 1.164654 seconds
Nov 29 10:55:20 tp1-systemd.lab chronyd[528]: Selected source 51.75.17.219
Nov 29 11:45:40 tp1-systemd.lab systemd[1]: Stopping NTP client/server...
Nov 29 11:45:40 tp1-systemd.lab chronyd[528]: chronyd exiting
Nov 29 11:45:40 tp1-systemd.lab systemd[1]: chronyd.service: Succeeded.
Nov 29 11:45:40 tp1-systemd.lab systemd[1]: Stopped NTP client/server.
[root@tp1-systemd ~]# wtf ?
Oui. 
Bug visiblement du à la box vagrant
[root@tp1-systemd ~]# cat /usr/lib/systemd/system/chronyd.service
[Unit]
Description=NTP client/server
Documentation=man:chronyd(8) man:chrony.conf(5)
After=ntpdate.service sntp.service ntpd.service
Conflicts=ntpd.service systemd-timesyncd.service
ConditionCapability=CAP_SYS_TIME

[Service]
Type=forking
PIDFile=/run/chrony/chronyd.pid
EnvironmentFile=-/etc/sysconfig/chronyd
ExecStart=/usr/sbin/chronyd $OPTIONS
ExecStartPost=/usr/libexec/chrony-helper update-daemon
PrivateTmp=yes
ProtectHome=yes
ProtectSystem=full

[Install]
WantedBy=multi-user.target
```

# Boot et Logs

`systemd-analyze plot > graphe.svg` permet de réaliser une courbe à partir des différentes métriques recueillies lors du boot :
![graph boot systemd-analyze](./graphe.svg)
On peut par exemple voir que `sshd` a mis 120ms à démarrer, environ 6,6 secondes après le boot.

On peut réaliser la même chose en ligne de commande et on trouve logiquement le même résultat pour `sshd`:
```
[root@fedora31 ~]# systemd-analyze blame
1min 4.271s unbound-anchor.service                                                   
    11.069s dnf-makecache.service                                                    
     1.503s sysstat-summary.service                                                  
     1.179s initrd-switch-root.service                                               
      866ms firewalld.service                                                        
      759ms docker.service                                                           
      648ms tuned.service                                                            
      502ms sssd.service                                                             
      476ms systemd-logind.service                                                   
      420ms systemd-journald.service                                                 
      375ms sysroot.mount                                                            
      372ms systemd-udevd.service                                                    
      343ms polkit.service                                                           
      335ms auditd.service                                                           
      308ms systemd-resolved.service                                                 
      272ms systemd-networkd.service                                                 
      240ms user@1001.service                                                        
      228ms systemd-vconsole-setup.service                                           
      174ms boot.mount                                                               
      150ms systemd-journal-flush.service                                            
      145ms systemd-tmpfiles-setup-dev.service                                       
      130ms initrd-parse-etc.service                                                 
      125ms systemd-remount-fs.service                                               
      120ms sshd.service                                                             
      119ms systemd-udev-trigger.service                                             
      110ms systemd-sysctl.service                                                   
      102ms systemd-tmpfiles-setup.service                                         
[...]
```

`systemd` embarque un démon de journalisation `journald`. Il centralise tous les logs de la machines de la façon la plus exhaustive possible. Il rend notamment possible la génération du graphe avec `systemd-analyze plot` en récupérant les logs au chargement du kernel.
Les logs sont stockés dans un format binaire inexploitable à la main. Plutôt relou. L'avantage est que les logs deviennent exploitables de façon très flexibles avec des commandes dédiées :
```
$ sudo journalctl -xe

# Logs kernel (similaire à dmesg)
$ sudo journalctl -k 

# Logs d'une unité spécifique ou process spécifique
$ sudo journalctl -xe -u sshd.service
$ sudo journalctl -xe _PID=1
# Pour plus de filtres
$ man systemd.journal-fields 

# Logs en temps réel
$ journalctl -xe -f

# Logs avec des ordres dans le temps
$ sudo journalctl -xe -u sshd.service --since yesterday
$ sudo journalctl -xe -u sshd.service  --since "2019-11-28 01:00:00"

# JSON output
$ sudo journalctl -xe -u sshd.service  --since "2019-11-28 01:00:00" --output=json
```

# Mécanismes manipulés par `systemd`
## Cgroups
Le terme *cgroup* désigne un mécanisme kernel de labellisation de processus et restriction de ressources appliquées aux processus.
En clair, cela permet de rassembler les processus en groupe de processus. Pour ensuite potentiellement appliquer des restrictions d'accès aux ressources de la machines comme :
* utilisation CPU
* utilisation RAM
* I/O disque
* utilisation réseau
* utilisation namespaces (later)

Les cgroups possèdent une structures arborescentes, comme une arborescende fichiers. Mais au lieu de ranger des fichiers, on range des processus :fire: :fire: :fire:
Les cgroups sont désormais profondément intégrés aux systèmes GNU/Linux et systemd a été construit avec les cgroups en tête.
* un scope systemd est un cgroup qui contient des processus non-gérés par systemd
* un slice systemd est un cgroup qui contient des processus directement gérés par systemd

commandes :
* `systemd-cgls` : affiche une structure arborescente des cgroups
* `systemd-cgtop`: affiche un affichage comme top avec l'utilisation des ressources en temps réel, par cgroups
* `ps -e -o pid,cmd,cgroup` : ajoute l'affichage des cgroups à ps
* `cat /proc/<PID>/cgroup`

On utilise cette arborescence pour aller voir directement les valeurs associées au cgroup de notre shell via l'api `/sys`
```

[papy@tp1-systemd ~]$ sudo systemd-cgls
Control group /:
-.slice
├─user.slice
│ └─user-1001.slice
│   ├─session-3.scope                   # cgroup de mon shell
│   │ ├─2107 sshd: papy [priv]
│   │ ├─2110 sshd: papy@pts/0
│   │ ├─2111 -bash                      # mon shell
│   │ ├─3715 sudo systemd-cgls
│   │ ├─3717 systemd-cgls
│   │ └─3718 less
│   └─user@1001.service
│     └─init.scope
│       ├─694 /usr/lib/systemd/systemd --user
│       └─696 (sd-pam)
├─init.scope
│ └─1 /usr/lib/systemd/systemd --switched-root --system --deserialize 29
└─system.slice
  ├─haveged.service
  │ └─440 /usr/sbin/haveged -w 1024 -v 1 --Foreground
  ├─containerd.service
  │ └─1963 /usr/bin/containerd
[...]
[papy@tp1-systemd ~]$ sudo cat /proc/2111/cgroup
11:pids:/user.slice/user-1001.slice/session-3.scope
10:devices:/user.slice
9:perf_event:/
8:freezer:/
7:cpu,cpuacct:/user.slice
6:blkio:/user.slice
5:cpuset:/
4:net_cls,net_prio:/
3:hugetlb:/
2:memory:/user.slice/user-1001.slice/session-3.scope
1:name=systemd:/user.slice/user-1001.slice/session-3.scope
0::/user.slice/user-1001.slice/session-3.scope
[papy@tp1-systemd ~]$ sudo cat /sys/fs/cgroup/memory/user.slice/user-1001.slice/session-3.scope/memory.max_usage_in_bytes 
167952384
```

Je n'ai pas réussi à modifier les différentes propriétés de cgroup :|
```
[papy@tp1-systemd ~]$ sudo man systemd.resource-control
[papy@tp1-systemd ~]$ sudo cat /sys/fs/cgroup/memory/user.slice/user-1001.slice/memory.max_usage_in_bytes 
937734144
[papy@tp1-systemd ~]$ sudo systemctl set-property user-1001.slice  MemoryMax=512M --runtime && \
                      sudo cat /sys/fs/cgroup/memory/user.slice/user-1001.slice/memory.max_usage_in_bytes
937734144
```

Le fichier correspondant est cependant bien créé dans le dossier `/etc/systemd/system-control/` :
```
[papy@tp1-systemd ~]$ sudo cat /etc/systemd/system.control/user-1001.slice.d/50-MemoryMax.conf 
# This is a drop-in unit file extension, created via "systemctl set-property"
# or an equivalent operation. Do not edit.
[Slice]
MemoryMax=536870912
[papy@tp1-systemd ~]$ sudo systemctl daemon-reload
[papy@tp1-systemd ~]$ sudo cat /sys/fs/cgroup/memory/user.slice/user-1001.slice/memory.max_usage_in_bytes 
937734144
```

## `dbus`
`dbus` est une technologie d'IPC : elle permet la communication entre les processus

```
[papy@tp1-systemd ~]$ sudo systemd-run --wait -t /bin/bash
Running as unit: run-u360.service
Press ^] three times within 1s to disconnect TTY.
[root@os-tp1-systemd /]# sudo systemctl status run-u360
● run-u360.service - /bin/bash
   Loaded: loaded (/run/systemd/transient/run-u360.service; transient)
Transient: yes
   Active: active (running) since Sat 2019-11-30 15:17:28 UTC; 5min ago
 Main PID: 4134 (bash)
    Tasks: 1 (limit: 2354)
   Memory: 2.6M
   CGroup: /system.slice/run-u360.service
           └─4134 /bin/bash
[...]
[root@os-tp1-systemd /]# cat /sys/fs/cgroup/memory/system.slice/run-u360.service/memory.max_usage_in_bytes 
3383296
```

là encore, pas réussi à modifier les valeurs pour le cgroup. Pas compris cette partie là.
```
[papy@tp1-systemd ~]$ sudo systemd-run -p MemoryMax=512M --wait -t /bin/bash
Running as unit: run-u411.service
Press ^] three times within 1s to disconnect TTY.
[root@os-tp1-systemd /]# sudo cat /sys/fs/cgroup/memory/system.slice/run-u411.service/memory.max_usage_in_bytes 
2932736
```

# `systemd` units in-depth

## Explorations de services existants

Observons l'unité de service de `auditd` :
```
papy@tp1-systemd ~]$ sudo systemctl status auditd
● auditd.service - Security Auditing Service
   Loaded: loaded (/usr/lib/systemd/system/auditd.service; enabled; vendor preset: enabled)
   Active: active (running) since Fri 2019-11-29 10:55:09 UTC; 1 day 4h ago
     Docs: man:auditd(8)
[...]
[papy@tp1-systemd ~]$ sudo cat /usr/lib/systemd/system/auditd.service
[Unit]
Description=Security Auditing Service
DefaultDependencies=no
## If auditd is sending or recieving remote logging, copy this file to
## /etc/systemd/system/auditd.service and comment out the first After and
## uncomment the second so that network-online.target is part of After.
## then comment the first Before and uncomment the second Before to remove
## sysinit.target from "Before".
After=local-fs.target systemd-tmpfiles-setup.service
##After=network-online.target local-fs.target systemd-tmpfiles-setup.service
Before=sysinit.target shutdown.target
##Before=shutdown.target
Conflicts=shutdown.target
RefuseManualStop=yes
ConditionKernelCommandLine=!audit=0
Documentation=man:auditd(8) https://github.com/linux-audit/audit-documentation

[Service]
Type=forking
PIDFile=/run/auditd.pid
ExecStart=/sbin/auditd
## To not use augenrules, copy this file to /etc/systemd/system/auditd.service
## and comment/delete the next line and uncomment the auditctl line.
## NOTE: augenrules expect any rules to be added to /etc/audit/rules.d/
ExecStartPost=-/sbin/augenrules --load
#ExecStartPost=-/sbin/auditctl -R /etc/audit/audit.rules
# By default we don't clear the rules on exit. To enable this, uncomment
# the next line after copying the file to /etc/systemd/system/auditd.service
#ExecStopPost=/sbin/auditctl -R /etc/audit/audit-stop.rules

### Security Settings ###
MemoryDenyWriteExecute=true
LockPersonality=true
ProtectControlGroups=true
ProtectKernelModules=true

[Install]
WantedBy=multi-user.target
```

* La clause `ExecStartPost` permet d'exécuter des actions à la fin du démarage d'un service, pour par exemple initialiser des données.  
* la clause `MemoryDenyWriteExecute` set à `true` empêche l'attribution d'emplacement RAM à la fois autorisés en écriture et en exécution. D'après la [doc](http://man7.org/linux/man-pages/man5/systemd.exec.5.html) "improves service security, as it makes harder for software exploits to change running code dynamically". 
* la clause `LockPersonality` empêche le change de `personality` du service. D'après la doc de [`personality`](http://man7.org/linux/man-pages/man2/personality.2.html), "Linux supports different execution domains, or personalities, for each process.  Among other things, execution domains tell Linux how to map signal numbers into signal actions."
* la clause `ProtectControlGroups` set à `true` rend le dossier `/sys/fs/cgroup/` accessible en lecture seule uniquement pour les processus et sous procesus du service.
* la clause `ProtectKernelModules` empêche le chargement explicite de modules kernel par le service.

## Création d'un service simple

```
[papy@tp1-systemd /]$ sudo cat /etc/systemd/system/flask.service
[Unit]
Description=Bap Bap dou Bap Bap
After=network.target

[Service]
Type=simple
User=papy
WorkingDirectory=/
PermissionsStartOnly=true
Environment=FLASK_APP=hello.py
ExecStart=/home/papy/.local/bin/flask run --host=0.0.0.0 --port=8000
ExecStartPre=/usr/bin/firewall-cmd --add-port=8000/tcp
ExecStopPost=/usr/bin/firewall-cmd --remove-port=8000/tcp
MemoryAccounting=true
MemoryHigh=25M
MemoryMax=50M

[Install]
WantedBy=multi-user.target
[papy@tp1-systemd /]$ sudo systemctl daemon-reload
[papy@tp1-systemd /]$ sudo systemctl start flask
[papy@tp1-systemd /]$ ss -laputn | grep flask
tcp    LISTEN  0       128               0.0.0.0:8000            0.0.0.0:*       users:(("flask",pid=22968,fd=3)) 
[papy@tp1-systemd /]$ journalctl -u flask
Nov 30 16:44:21 os-tp1-systemd firewall-cmd[22967]: success
Nov 30 16:44:21 os-tp1-systemd flask[22968]:  * Serving Flask app "hello.py"
Nov 30 16:44:21 os-tp1-systemd flask[22968]:  * Environment: production
Nov 30 16:44:21 os-tp1-systemd flask[22968]:    WARNING: This is a development server. Do not use it in a production deployment.
Nov 30 16:44:21 os-tp1-systemd flask[22968]:    Use a production WSGI server instead.
Nov 30 16:44:21 os-tp1-systemd flask[22968]:  * Debug mode: off
Nov 30 16:44:21 os-tp1-systemd flask[22968]:  * Running on http://0.0.0.0:8000/ (Press CTRL+C to quit)
Nov 30 16:44:21 os-tp1-systemd firewall-cmd[22969]: success
----------------------------------------------------------------------------------------------------------------------------------
# papy at La-Bretzelerie in ~/ :
→ curl 192.168.100.22:8000
Hello, World!
```


